package org.alexd.testfx.temp

import arrow.core.ListK
import arrow.core.extensions.listk.traverse.traverse
import arrow.core.k
import arrow.fx.IO
import arrow.fx.extensions.fx
import arrow.fx.extensions.timer
import arrow.fx.fix
import arrow.fx.typeclasses.seconds
import kotlinx.coroutines.delay

suspend fun putStrLn(s: String) { println(s) }

suspend fun log(s: String) {
    val t = getTime()
    println("$t: $s")
}

suspend fun getTime() = System.currentTimeMillis()


fun <T> benchWork(name: String, f: () -> IO<T>) = IO.fx {
    !effect { putStrLn("$name started") }
    val startTime = !effect { getTime() }
    val (res) = f()
    val duration = !effect { getTime() } - startTime
    !effect { putStrLn("$name ended ($duration ms)") }
    res
}


fun work(name: String): IO<String> = IO.fx {
    !effect {
        println("Start $name")
    }

    !effect {
        if (name == "Task 0") {
            delay(200)
            putStrLn("Fail !!!")
            error("Something went wrong")
        }
    }

    !IO {
        println("Acquiring resource for $name")
        name
    }.bracket({ IO { println("Releasing resource for $it")}}, { a ->
        IO.fx {
            !effect { putStrLn("Start work $a")}
            !IO.timer().sleep(1.seconds)
            !effect { putStrLn("End work $a")}
        }
    })

    !effect {
        println("End $name")
    }

    name
}


fun performWorkInParallel() = IO.fx {
    val tasks = (0 until 50).map { work("Task $it") }

    println("test")
    val res = !benchWork("parSequence") {
        tasks.k().parSequenceN(ListK.traverse(), 10).fix()
    }

    !effect { putStrLn(res.toString()) }
    res
}



fun main() {
//    println(performWorkInParallel().unsafeRunSync())
//    Thread.sleep(1000)

//    performWorkInParallel().unsafeRunAsync {
//        println("Result $it")
//    }
//    Thread.sleep(2000)

    //val nonCancellable = performWorkInParallel().unsafeRunSync()


    val cancellable = performWorkInParallel().unsafeRunAsyncCancellable {
        println("Result $it")
    }
    Thread.sleep(100)
    //cancellable()
//    print("Cancelled")
    Thread.sleep(50000)
}